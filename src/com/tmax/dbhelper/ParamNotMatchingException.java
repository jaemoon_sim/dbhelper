package com.tmax.dbhelper;

public class ParamNotMatchingException extends Exception{
	ParamNotMatchingException(String msg){
		super(msg);
	}
}
