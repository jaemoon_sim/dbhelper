package com.tmax.dbhelper;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import com.tmax.promapper.engine.dto.record.common.FieldProperty;
import com.tmax.proobject.dataobject.session.DBSession;
import com.tmax.proobject.dataobject.session.SessionContainer;
import com.tmax.proobject.logger.ProObjectLogger;
import com.tmax.proobject.logger.application.ServiceLogger;
import com.tmax.proobject.model.dataobject.DataObject;
import com.tmax.proobject.model.exception.FieldNotFoundException;
import com.tmax.proobject.util.dataobject.StringUtils;

public class DBHelper {
	ProObjectLogger mLogger = ServiceLogger.getLogger();
	
	public DBHelper() {
	};
	
	/**
	 * @return Connection  DB(tibero6)���� connection
	 * @throws ClassNotFoundException 
	 */
	public Connection getDBConnection() throws ClassNotFoundException {
		SessionContainer sessionContainer = SessionContainer.getInstance();
		if(sessionContainer == null) {
			log("Failed to get connection from SessionContainer");
			return null;		
		}
		DBSession session = (DBSession) sessionContainer.getSession();
		if(session == null) {
			log("Failed to get connection from SessionContainer");
			return null;
		}
		Connection conn = session.getConnection(); 
		return conn;
	}
	
	/**
	 * @param ip DB(tibero6)�� ip:port
	 * @param id �α��� id
	 * @param pw �α��� pw
	 * @return Connection DB(tibero6)���� Connection
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public Connection createDBConnection(String ip, String id, String pw) throws ClassNotFoundException, SQLException {
		Connection conn = null;
		Class.forName("com.tmax.tibero.jdbc.TbDriver");
		conn = DriverManager.getConnection(
				"jdbc:tibero:thin:@"+ip+":tibero",
				id, pw);
		if (conn == null)
		{
			log("Failed to create connection to "+ip);
			return null;

		}
		log("Connection success!");	
		return conn;
	}
	

	private void log(String msg) {
		msg = "[DBHelper] "+msg;
		if(mLogger==null) {
			System.out.println(msg);
		}else {
			mLogger.log(Level.INFO, msg);
		}	
	}
	
	private String createPlainQuery(String sql, Object... params) {
		String splitted[] = sql.split("\\?");

		String result = splitted[0];
		int idx = 1;
		
		for(Object param : params) {
			if(param == null) {
				result = result + "NULL" ;
			}else {
				Class<? extends Object> paramClass = param.getClass();
				if(paramClass == String.class) {
					String sParam = ((String)param).replaceAll("'", "''");
					result = result + "'"+sParam+"'";
				}else if(paramClass == Integer.class) {
					result = result + String.valueOf( (Integer)param );
				}else {
					result = result + "'"+param.toString()+"'";
				}
			}
			if(idx < splitted.length) {
				result = result + splitted[idx++];
			}else {
				break;
			}

		}
		log("created plain query >> "+result);
		return result;
	}

	/**
	 * @param sql query string
	 * @return ResultSet 
	 * getDBConnection()�� ���� ����� Connection�� sql����
	 * @throws SQLException query ����
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public ResultSet executeQuery(String sql) throws SQLException, ClassNotFoundException{
		Connection conn = this.getDBConnection(); 
		return this.executeQuery(conn, sql);
	}
	
	/**
	 * @param conn DB���� connection (createDBConnection(), getDBConnection()�� �̿��� �����)
	 * @param sql query string
	 * @return ResultSet
	 * Connection conn�� �̿��� sql ����
	 * @throws SQLException query ����
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public ResultSet executeQuery(Connection conn, String sql) throws SQLException, ClassNotFoundException{
		PreparedStatement pstmt = null;
		pstmt = conn.prepareStatement(sql);
		return pstmt.executeQuery();
	}
	
	/**
	 * @param sql ?�� ���Ե� sql 
	 * @param params parameters for sql
	 * @return ResultSet
	 * getDBConnection()�� �̿��� ����� Connection�� ?�� params�� ä�� sql ����
	 * @throws SQLException query ����
	 * @throws ParamNotMatchingException sql�� ?������ params ������ �ȸ���
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public ResultSet executeQuery(String sql, Object... params) throws SQLException, ParamNotMatchingException, ClassNotFoundException{
		Connection conn = this.getDBConnection();		
		return executeQuery(conn, sql, params);
	}
	/**
	 * @param conn DB���� connection (createDBConnection(), getDBConnection()�� �̿��� �����)
	 * @param sql query string
	 * @param params parameters for sql
	 * @return ResultSet
	 * Connection conn�� �̿��� ?�� params�� ä�� sql ����
	 * @throws SQLException query ����
	 * @throws ParamNotMatchingException sql�� ?������ params ������ �ȸ���
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public ResultSet executeQuery(Connection conn, String sql, Object... params) throws SQLException, ParamNotMatchingException, ClassNotFoundException{
		if(StringUtils.countOccurrencesOf(sql, "?") != params.length) 
			throw new ParamNotMatchingException("Param count does not match");
		
		sql = createPlainQuery(sql, params);
		return executeQuery(conn, sql);
	}
	
	/**
	 * @param sql query string
	 * @return int (success = 1, fail = 0) 
	 * getDBConnection()�� ���� ����� Connection�� sql����
	 * @throws SQLException query ����
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public int executeUpdate(String sql) throws SQLException, ClassNotFoundException{
		Connection conn = this.getDBConnection(); 
		return executeUpdate(conn, sql);
	}

	/**
	 * @param conn DB���� connection (createDBConnection(), getDBConnection()�� �̿��� �����)
	 * @param sql query string
	 * @return int (success = 1, fail = 0) 
	 * Connection conn�� �̿��� sql����
	 * @throws SQLException query ����
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public int executeUpdate(Connection conn, String sql) throws SQLException, ClassNotFoundException{
		PreparedStatement pstmt = null;
		pstmt = conn.prepareStatement(sql);
		return pstmt.executeUpdate();
	}
	
	/**

	 */
	public int executeUpdate(String sql, Object... params) throws ParamNotMatchingException, SQLException, ClassNotFoundException {
		Connection conn = getDBConnection();
		return executeUpdate(conn, sql, params);

	}
	
	/**
	 * @param conn DB���� connection (createDBConnection(), getDBConnection()�� �̿��� �����)
	 * @param sql query string
	 * @param params parameters for sql
	 * @return int (success = 1, fail = 0) 
	 * Connection conn�� �̿��� ?�� params�� ä�� sql ����
	 * @throws SQLException query ����
	 * @throws ParamNotMatchingException sql�� ?������ params ������ �ȸ���
	 * @throws ClassNotFoundException getDBConnection() ����
	 */
	public int executeUpdate(Connection conn, String sql, Object... params) throws ParamNotMatchingException, SQLException, ClassNotFoundException {
		if(StringUtils.countOccurrencesOf(sql, "?") != params.length) 
			throw new ParamNotMatchingException("Param count does not match");
		sql = createPlainQuery(sql, params);
		return executeUpdate(conn, sql);
	}
	
	
	private ArrayList<String> createColumnList(DataObject param) {
		ArrayList<String> columnList = new ArrayList<String>();
		
		@SuppressWarnings("unchecked")
		Map<String, FieldProperty> fieldMap = param.getFieldPropertyMap();
		Iterator<String> itr = fieldMap.keySet().iterator();
				
		while(itr.hasNext()) {
			String key = itr.next();
			FieldProperty val = fieldMap.get(key);
			
			String logicName = val.getLogicalName(); 
						
			Object object = param.get(logicName);
			if(object == null) continue;
			try {
				Class<? extends Object> type = object.getClass();
				if(type == String.class) {
					if( ((String)object).equals("") ) {
						continue;
					}
				}else if(type == Character.class) {
					if( ((Character)object) == java.lang.Character.MIN_VALUE ) continue;
				}
				
			} catch( NullPointerException e) {
				continue;
			}
			
			columnList.add(logicName);
		}			

		return columnList;
	}
	
	/**
	 * @param action INSERT or UPDATE
	 * @param table SchemaObject.TABLE_NAME
	 * @param param query�� �����ϰ� ���� DataObject
	 * param�� META�� META ���� �޾ƿͼ� INSERT Ȥ�� UPDATE query�� ����
	 * META���� null�̰ų� �� ĭ("", 0 ��)�� ���, �ش� META�� query�� ���Ե��� ����
	 * @return String ������ query
	 */
	public String createQuery(String action, String table, DataObject param) {
		if(table.contains(" ") || table.contains(";"))
			return null;
		
		if(action.toUpperCase().equals("INSERT")) {
			return createInsertQuery(table, param);
		}
		
		if(action.toUpperCase().equals("UPDATE")) {
			return createUpdateQuery(table, param);
		}
		
		
		return null;
	}
	
	/**
	 * @param action INSERT or UPDATE
	 * @param table SchemaObject.TABLE_NAME
	 * @param columns query�� �����ϰ� ���� column�� list
	 * @param param query�� �����ϰ� ���� DataObject
	 * param�� columns�� item���� �̿�, columns�� item�� �̸��� ���� param�� META ���� �޾ƿͼ� INSERT Ȥ�� UPDATE query�� ����
	 * @return String ������ query
	 */
	public String createQuery(String action, String table, ArrayList<String> columns, DataObject param) {
		if(table.contains(" ") || table.contains(";"))
			return null;
		
		if(action.toUpperCase().equals("INSERT")) {
			return createInsertQuery(table, columns, param);
		}
		
		if(action.toUpperCase().equals("UPDATE")) {
			return createUpdateQuery(table, columns, param);
		}
		
		
		return null;
	}
	
	private String createInsertQuery(String table, DataObject param) {
		ArrayList<String> columnList = createColumnList(param);
		return createInsertQuery(table, columnList, param);
	}
	
	private String createInsertQuery(String table, ArrayList<String> columns, DataObject param) {
		ArrayList<String> columnList = columns;
		
		StringBuilder sb = new StringBuilder("INSERT INTO ");
		sb.append(table);
		sb.append("(");
		
		int columnCount = columnList.size();
		//Start writing column names;
		for(int i = 0 ; i < columnCount ; i++){
			sb.append(columnList.get(i));
			if( i < columnCount-1 )
				sb.append(",");
		}
		sb.append(") VALUES ");
		sb.append(createInsertValues(columnList, param));
	
		log("created query :"+sb.toString());
		return sb.toString();
	}
	
	private String createInsertValues(ArrayList<String> columnList, DataObject param) {
		StringBuilder sb = new StringBuilder("(");
		
		int columnCount = columnList.size();
		
		for(int i = 0 ; i < columnCount ; i++) {
			sb.append(getMeta(param, columnList.get(i)));
			if(i < columnCount-1 )
				sb.append(",");	
		}
		sb.append(")");
		
		return sb.toString();
	}
	
	private String createUpdateQuery(String table, DataObject param) {
		ArrayList<String> columnList = createColumnList(param);
		return createUpdateQuery(table, columnList, param);
	}
	
	private String createUpdateQuery(String table, ArrayList<String> columns, DataObject param) {
		ArrayList<String> columnList = columns;
		
		StringBuilder sb = new StringBuilder("UPDATE ");
		sb.append(table);
		sb.append(" SET ");
		
		int columnCount = columnList.size();
		
		for(int i = 0 ; i < columnCount ; i++){
			sb.append(columnList.get(i));
			sb.append("=");
			sb.append(getMeta(param, columnList.get(i)));
			if( i < columnCount-1 )
				sb.append(",");
		}
		
		
		log("created query :"+sb.toString());
		return sb.toString();
	}
	
	public String getMeta(DataObject param, String columnName) {
		Object item = param.get( columnName );
		if(item == null) {
			return "null";
		}
		Class<? extends Object> type = item.getClass();
//		mLogger.log(Level.INFO, "[DBHelper - getMeta] item class:"+type.toString());
		if(type == Integer.class) {
			return String.valueOf((int)item);
		}else if(type == String.class) {
			if( ((String)item).contains(";") )
				return "";
			if( ((String)item).toUpperCase().equals("SYSDATE") ) {
				return ((String)item);
			}else {
				return "'"+(String)item+"'";
			}
		}else if(type == java.util.Date.class) {
			
			return "'"+(new java.sql.Date(((java.util.Date)item).getTime())).toString()+"'";
		}else if(type==java.security.Timestamp.class  || type == java.sql.Timestamp.class){
			return "'"+item.toString()+"'";
		}else if(type == java.lang.Character.class) {
			return "'"+item+"'";
		}
		return "";
	}
	
	/***
	 * @param outputType output�� type���μ� output�� Ÿ��.class�� �־��ָ� ��. e.g., PIMChannelDO.class
	 * @param rs ResultSet�� Cursor. �ڵ����� next()���ִ� ����� ���Ե��� ����. DO�� meta���� �̸��� column �̸��� ���ٴ°� ������.
	 * @return DataObject
	 * @throws InstantiationException
	 * @throws IllegalAccessException
	 * @throws SQLException 
	 * @throws FieldNotFoundException 
	 */
	@SuppressWarnings("unchecked")
	public <T extends DataObject> T fillDO(Class<? extends DataObject> outputType, ResultSet rs) 
			throws InstantiationException, IllegalAccessException, FieldNotFoundException, SQLException {
		
		
		log("fillDO called");
		T result = (T) new DOContainer<T>().createDO((Class<T>) outputType);
		ResultSetMetaData rsmd = rs.getMetaData();
		int columns = rsmd.getColumnCount();
		Map<String, FieldProperty> doMap = result.getFieldPropertyMap();
		Set<String> metaSet = doMap.keySet();
		for(int i=1; i<columns+1; i++) {
			 if(!metaSet.contains(rsmd.getColumnName(i))) {
				 continue;
			 }
			 switch(rsmd.getColumnType(i)) {
			 	case java.sql.Types.NUMERIC:
			 		result.set(rsmd.getColumnName(i), rs.getInt(i));	
			 		break;
			 	case Types.CHAR:
			 		char charItem = rs.getString(i)==null?null:rs.getString(i).charAt(0);
			 		result.set(rsmd.getColumnName(i), charItem);
			 		break;
			 	case Types.VARCHAR:
			 		result.set(rsmd.getColumnName(i), rs.getString(i));
			 		break;
		 		case Types.TIMESTAMP:
		 			Date date = rs.getDate(i);
		 			try {
		 			if(date != null)
		 				result.set(rsmd.getColumnName(i), new java.util.Date(date.getTime()));
		 			}catch(java.lang.ClassCastException e) {
		 				result.set(rsmd.getColumnName(i), rs.getTimestamp(i));
		 			}
		 			break;
		 		
			 }
			 
		 }
		 return result;
	
	}
	
	private static class DOContainer<E extends DataObject>{
		E createDO(Class<E> input) throws InstantiationException, IllegalAccessException {
			return input.newInstance();
		}
	}
	
}
